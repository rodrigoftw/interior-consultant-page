# Interior Consultant Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://distracted-nightingale-7d587f.netlify.app/) | [Solution](https://devchallenges.io/solutions/x4icefZNvJVRPx4Qc18n) | [Challenge](https://devchallenges.io/challenges/Jymh2b2FyebRTUljkNcb) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FinteriorConsultantThumbnail.png%3Falt%3Dmedia%26token%3Dfb5f8229-8eb9-4d70-b0d4-fbd2829a0cf0&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/Jymh2b2FyebRTUljkNcb) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

## Features

This project features:
- a responsive view between:
  - a laptop (large) view;
  - a desktop (4K) view;
- a mobile view of the page with a hamburger menu.


## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
