const headerButton = document.getElementById('header-button');
const hamburger = document.getElementById('hamburger');
const navUl = document.getElementById('nav-ul');

hamburger.addEventListener('click', () => {
  switch (hamburger.innerHTML) {
    case "menu":
      headerButton.style.visibility = "hidden";
      hamburger.innerHTML = "close";
      break;
    case "close":
      headerButton.style.visibility = "visible";
      hamburger.innerHTML = "menu";
      break;

    default:
      headerButton.style.visibility = "visible";
      hamburger.innerHTML = "menu";
      break;
  }

  navUl.classList.toggle('show');
})